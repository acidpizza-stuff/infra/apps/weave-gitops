# Weave GitOps

[[_TOC_]]


## Installation

Reference: https://docs.gitops.weave.works/docs/installation/weave-gitops/

```bash
# Install gitops CLI
version="v0.24.0"
curl --silent --location "https://github.com/weaveworks/weave-gitops/releases/download/${version}/gitops-$(uname)-$(uname -m).tar.gz" | tar xz -C /tmp
sudo mv /tmp/gitops /usr/local/bin
gitops version

# Show draft of flux resources for helm installation
gitops create dashboard weave-gitops --export > weave-gitops.yaml

# Install secret for admin user
read -s PASSWORD
kubectl -n weave-gitops create secret generic cluster-user-auth \
  --from-literal=username=admin \
  --from-literal=password="$(echo -n $PASSWORD | gitops get bcrypt-hash)"
```


## Update

Check the [charts page](https://github.com/weaveworks/weave-gitops/pkgs/container/charts%2Fweave-gitops) to find the latest version to upgrade to.